

@if($service != null)
    @if(!isset($service->title))
    <div class="{{$class}}" style="background: url({{ asset("img/aditional-services-background.jpg") }});">
        <div class="container">
            <div class="row about-company-row">
                <div class="col">
                    <div class="about-company">
                        <h1> Дополнительные услуги</h1>
                    </div>
                    <div class="breadcrumbs">
                        <a href="{{ route('home') }}" style="color: #FFFFFF;text-align: center;">Главная</a>
                        <span class="chevron"></span>
                        <a href="{{ route('all_service') }}" style="color: #FFFFFF;text-align: center;">Услуги</a>
                        <span class="chevron"></span>  Дополнительные услуги
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
        <div class="{{$class}}" style="background:url({{Voyager::image($service->banner)}});background-size: cover;background-position: center center;">
            <div class="container">
                <div class="row about-company-row">
                    <div class="col">
                        <div class="about-company">
                            <h1>{{$page->getPage()->title}}</h1>
                        </div>
                        <div class="breadcrumbs">
                            <a href="{{ route('home') }}" style="color: #FFFFFF;text-align: center;">Главная</a>
                            <span class="chevron"></span>
                            <a href="{{ route('all_service') }}" style="color: #FFFFFF;text-align: center;">Услуги</a>
                            <span class="chevron"></span> {{$page->getPage()->title}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@elseif($page->getPage()->title != null)
    <div class="{{$class}}" style="background: linear-gradient(rgba(12, 13, 37, 0.35), rgba(12, 13, 37, 0.35)),
            url({{Voyager::image($page->getPage()->banner)}});">
        <div class="container">
            <div class="row about-company-row">
                <div class="col">
                    <div class="about-company">
                        <h1>{{$page->getPage()->title}}</h1>
                    </div>
                    <div class="breadcrumbs">
                        <a href="{{ route('home') }}" style="color: #FFFFFF;text-align: center;">Главная</a>
                        <span class="chevron"></span> {{$page->getPage()->title}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($blog != null)
    <div class="{{$class}}"  style="background: linear-gradient(rgba(12, 13, 37, 0.35), rgba(12, 13, 37, 0.35)),
            url({{Voyager::image($page->getPage()->banner)}});">
        <div class="container">
            <div class="row about-company-row">
                <div class="col">
                    <div class="about-company">
                        <h1>{{ $page->getPage()->name }}</h1>
                    </div>
                    <div class="breadcrumbs">
                        <a href="{{ route('home') }}" style="color: #FFFFFF;text-align: center;">Главная <span class="chevron"></span></a>
                        <a href="{{ route('all_blog') }}" style="color: #FFFFFF;text-align: center;">Статьи и новости <span class="chevron"></span> </a>
                        {{ $page->getPage()->name }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

