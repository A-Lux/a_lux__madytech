@php $m=0 @endphp
@foreach($clients as $k => $v)
    @php $m++ @endphp
    @if($m % 4 == 1)
        <div class="row {{$m == 1 ? "happy-client":""}}">
            @endif
            <div class="col-sm col-6">
                <div class="client-logo-wrapper">
                    <img src="{{Voyager::image($v->image)}}" alt="">
                </div>
                <div class="client-logo-wrapper-substrate"></div>
            </div>
            @if($m % 4 == 0)
        </div>
    @endif
@endforeach
@if($m % 4 != 0)
    </div>
@endif
