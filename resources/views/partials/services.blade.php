<div class="service-page-button-links-wrapper row">
    <nav class="nav align-middle nav-pills flex-column flex-md-row">
        @foreach($services as $k => $v)
            <a href="{{ route('inner_service', $v->url) }}" class="flex-sm-fill text-sm-center nav-link
                {{ $type == $v->id ? "button-link-active" : "" }}" >{{$v->name}}</a>
        @endforeach
        <a href="{{ route('inner_service', 'additional') }}" class="flex-sm-fill text-sm-center nav-link
            {{ $type == "additional" ? "button-link-active" : "" }}">Дополнительные услуги</a>
    </nav>
</div>
