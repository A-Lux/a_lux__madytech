<div class="form-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-4 form-wrapper-header-for-mobile">Хотите провести обучение по охране труда в
                Алматы?</div>
        </div>
        <div class="row">
            <div class="col-sm-5 col-xl-6 offset-4 form-wrapper-header-for-mobile">
                <h2 class="h2-normal-style">Оставляйте заявку сейчас</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 form-wrapper-background-for-mobile">
                <div class="form-women-background"></div>
            </div>
            <div class="col-md-8 form-wrapper-section-for-mobile">
                <form>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputName">Как вас зовут?</label>
                            <input type="text" class="form-control" id="inputName"
                            name="fio">
                        </div>
                        <div class="form-group col-md-6 form-col-margin">
                            <label for="inputEmail">Ваш e-mail</label>
                            <input type="email" class="form-control" id="inputEmail" placeholder="" name="email">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputGoalOfEducation">Введите сообщение</label>
                            <input type="text" class="form-control" id="inputGoalOfEducation" name="sms">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <div class="form-check">
                                <button type="button" id="requestBtn"
                                        class="btn btn-primary form-button form-my-own-button">Отправить</button>
                            </div>
                        </div>
                        <div class="form-group form-check-content col-sm-5">
                            <input class="form-check-input form-my-own-checkbox" name="agreement" type="checkbox" id="gridCheck">
                            <label class="form-check-label form-my-own-checkbox-text" for="gridCheck">
                                Нажимая на кнопку вы соглашаетесь
                                на обработку персональных данных
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
