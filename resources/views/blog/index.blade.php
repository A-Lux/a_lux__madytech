@extends('layouts.app')
@section('content')

    @include('partials.banners',['class' => "service-page-wrapper blog-wrapper"])
    {{--@include('partials.banners',['class' => "service-page-wrapper about-blog"])--}}
    <div class="blog-page">
        <div class="container">
            <h4>{{ $model->name }}</h4>
            <div class="blog-date">
                <p><i class="far fa-clock"></i>{{ $model->created_at }}</p>
            </div>

           {!! $model->full_description !!}

            @if($recommends != null)
                <div class="related-posts">
                    <h3>Похожие новости</h3>
                    <div class="row">
                        @foreach($recommends as $k => $v)
                            <div class="col-xl-4 related-content">
                                <img src="{{ Voyager::image($v->news->image) }}" alt="">
                                <h3>{{ $v->news->name }}</h3>
                                <a href="{{ route('inner_blog', $v->news->url) }}"><button>Читать далее</button></a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection
