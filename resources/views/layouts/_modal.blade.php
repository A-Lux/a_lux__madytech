
<div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="feedbackModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requestVacancyModalLabel">Заказать звонок</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">

                    <label for="recipient-name" class="col-form-label">Ваше имя:</label>
                    <input type="text" class="form-control" id="name" name="name">

                    <label for="message-text" class="col-form-label">Ваш телефон:</label>
                    <input class="form-control" id="telephone" name="telephone" placeholder="Например: +77087772757">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" id="feedbackBtn" style="background: #242940;border-radius: 100px;">Жду звонка</button>
            </div>
        </form>
    </div>
</div>
