@extends('layouts.app')
@section('content')

    @include('partials.banners', ['class' => 'service-page-wrapper electrical-safety-wrapper'])
    @include('partials/services', ['type' => $service->id])

    <div class="container">
        @foreach($blocks as $k => $v)
            <div class="row {{ $k > 1 ? "page-service-learning" : "page-service-kwnoladge" }}">
                <div class="col">
                    <h2 class="h2-normal-style">{{ $v->title }}</h2>
                    {!!  $v->content !!}
                    @if($v->btn != "")
                    <a href="{{ $v->url }}">
                        <button type="button" class="service-page-button-test"><p>{{ $v->btn }}</p></button></a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>

    <noindex>
        <div class="personal-education-wrapper">
            <div class="container">

                <div class="row personal-education-row">
                    <h2 class="h2-white-style personal-education-row-h2">Программы обучения</h2>
                    <p class="personal-education-p">{{ $studying_program_con != null ? $studying_program_con->text : "" }}</p>
                </div>

                <div class="row personal-education-cart-row">
                    @php $m = 0; @endphp
                    @foreach($studying_programs as $k => $v)
                        @php $m++; @endphp
                        <div class="col-sm col-md-6 col-xl-3" id="{{ $v->id }}">
                            <div class="personal-education-cart personal-education-cart-{{ numberToWords($m) }}-background">
                                <h5>{{ $v->name }}</h5>
                                @if($v->duration != null)
                                    <p class="p-grey-style"><img src="{{ asset("img/personal-education-cart-clock.png") }}" class="personal-education-cart-clock"  alt="">
                                        Длительность {!!  $v->duration !!} часа</p>
                                @endif
                                <div class="personal-education-cart-description">
                                    {!! $v->content !!}
                                </div>
                            </div>
                            <div class="personal-education-cart-link personal-education-cart-{{ numberToWords($m) }}-background-link">
                                <a style="cursor: pointer;" onclick="showProgramForm({{ $v->id }})">Выбрать программу <span class="chevron"></span></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="benefits-of-our-education-wrapper">
            <div class="container">
                <div class="row benefits-of-our-education-title-row">
                    <h2 class="h2-normal-style">Приемущества обучения у нас</h2>
                </div>
                <div class="benefits-of-our-education-parent">
                    <div class="row">
                        @foreach($advantages as $k => $v)
                            <div class="col-sm benefits-of-our-education-parent-border">
                                <div class="benefit-of-our-education-image"><img
                                            src="{{ Voyager::image($v->image) }}" alt=""></div>
                                <div class="benefit-of-our-education-title">{{ $v->name }}</div>
                                <div class="benefit-of-our-education-description">{!! $v->content !!}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row stages-of-cooperation">
                <div class="col">
                    <h2 class="h2-normal-style">Этапы сотрудничества</h2>
                </div>
            </div>
            @php $m = 0; @endphp
            @foreach($stages as $k => $v)
            @php $m++; @endphp
            @if($m % 3 == 1)
            <div class="row cooperation-row">
            @endif
                <div class="col-md-4">
                    <div class="stage-of-cooperation-wrapper">
                        <div class="stage-of-cooperation-number">0{{ $m }}</div>
                        <p>{!! $v->content !!}</p>
                    </div>
                </div>
            @if($m % 3 == 0)
            </div>
            @endif
            @endforeach
            @if($m % 3 != 0)
            </div>
            @endif
        </div>
    </noindex>


    <div class="form-wrapper" id="programForm" rel="nofollow">
        <div class="container">
            <div class="row"><div class="col-md-4 offset-4 form-wrapper-header-for-mobile">Хотите провести обучение по охране труда в Алматы?</div></div>
            <div class="row"><div class="col-sm-5 offset-4 form-wrapper-header-for-mobile"><h2 class="h2-normal-style">Оставляйте заявку сейчас</h2></div> </div>
            <div class="row">
                <div class="col-md-4 form-wrapper-background-for-mobile">
                    <div class="form-women-background"></div>
                </div>
                <div class="col-md-8 form-wrapper-section-for-mobile">
                    <form >
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputName">Как вас зовут?</label>
                                <input name="fio" type="text" class="form-control" id="inputName" placeholder="Сорокина Маргарита Андреевна">
                            </div>
                            <div class="form-group col-md-6 form-col-margin">
                                <label for="inputEmail">ваш e-mail</label>
                                <input name="email" type="email" class="form-control" id="inputEmail" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputGoalOfEducation">Цель обучения</label>
                                <input name="training_purpose" type="text" class="form-control" id="inputGoalOfEducation">
                            </div>
                            <div class="form-group col-md-6 form-col-margin">
                                <label for="inputPersonalEducation">Выберите программу обучения</label>
                                <select id="inputPersonalEducation" class="form-control" name="service_studying_program_id">
                                    @php $type = "selected" @endphp
                                    @foreach($studying_programs as $k => $v)
                                        <option {{ $type }} value="{{ $v->id }}">{{ $v->name }}</option>
                                        @php $type = "" @endphp
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <div class="form-check">
                                    <button type="button" class="btn btn-primary form-button form-my-own-button"
                                    id="serviceRequestBtn">Отправить</button>
                                </div>
                            </div>
                            <div class="form-group form-check-content col-sm-5">
                                <input class="form-check-input form-my-own-checkbox" type="checkbox" id="gridCheck" name="agreement">
                                <label class="form-check-label form-my-own-checkbox-text" for="gridCheck">
                                    Нажимая на кнопку вы соглашаетесь
                                    на обработку
                                    @if(setting('site.agreement'))
                                    <a href="{{ setting('site.agreement') }}" target="_blank">персональных данных</a>
                                    @else
                                        персональных данных
                                    @endif
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
