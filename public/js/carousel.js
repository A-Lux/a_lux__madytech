
  $('.burger-menu-btn').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('burger-menu-lines-active');
    $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
    $('body').toggleClass('body-overflow');
  });

  $('.gallery').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 568,
        settings: {
          arrows: false,
          centerMode: false,
          // centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 1024,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 2
        }
      },
        {
          breakpoint: 1360,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
          }
      }
    ]
  });
  

$('.owl-carousel').owlCarousel({
  loop:true,
  margin:10,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:3
      },
      1000:{
          items:3
      }
  }
})
	
  $('.magific-pop').magnificPopup({
    type: 'image',
    zoom: {
        enabled: true,
        duration: 300,
    }
});