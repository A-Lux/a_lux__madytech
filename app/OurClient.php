<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OurClient extends Model
{
    public static function getAll(){
        return OurClient::orderBy('sort', 'ASC')->get();
    }

}
