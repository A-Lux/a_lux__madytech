<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class LawType extends Model
{

    public static function getAll(){
        return LawType::orderBy('sort', 'ASC')->with('laws')->get();
    }

    public function laws() {
        return $this->hasMany(Law::class);
    }
    
}
