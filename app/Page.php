<?php

namespace App;

use App\Page\IPage;
use Illuminate\Database\Eloquent\Model;


class Page extends Model implements IPage
{
    public static function getByUrl($url){
        return Page::where("url", $url)->first();
    }
}
