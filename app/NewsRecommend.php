<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class NewsRecommend extends Model
{
    public static function getByNews($news_id){
        return NewsRecommend::where("news_id", $news_id)->get();
    }

    public function news(){
        return $this->hasOne('App\News', 'id', 'recommend_news_id');
    }
}
