<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ServiceStudyingProgram extends Model
{
    public static function getByService($service_id){
        return ServiceStudyingProgram::where('service_id', $service_id)->orderBy('sort', 'ASC')->get();
    }
    
}
