<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CompanyInNumber extends Model
{

    public static function getAll(){
        return CompanyInNumber::orderBy('sort', 'ASC')->get();
    }
}
