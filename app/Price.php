<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Price extends Model
{
    public function prices() {
        return $this->hasMany(PriceList::class)->orderBy("sort", 'ASC');
    }


    public function units() {
        return $this->hasMany(PriceUnit::class)->orderBy("sort", 'ASC');
    }
}
