<?php

namespace App\Providers;

use App\AdditionalService;
use App\MainTitle;
use App\News;
use App\Page\Page;
use App\Service;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Page $page, Service $service, MainTitle $title, AdditionalService $additionalService, News $blog)
    {

        View::composer('*', function($view) use($page) {
            $view->with(['page' => $page]);
        });


        View::composer('*', function($view) use($service) {
            $view->with(['services' => $service::getAll()]);
        });


        View::composer('*', function($view) use($title) {
            $view->with(['title' => $title::getAll()]);
        });


        View::composer('*', function($view) use($service, $additionalService) {
            $service = $service::getByUrl( Request::segment(2));
            if(!$service) $service = $additionalService::getByUrl( Request::segment(2));
            $view->with(['service' => $service]);
        });


        View::composer('*', function($view) use($blog) {
            $blog = $blog::getByUrl( Request::segment(1));
            $view->with(['blog' => $blog]);
        });








    }
}
