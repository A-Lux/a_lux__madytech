<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.02.2020
 * Time: 11:02
 */

namespace App\Http\Controllers;


use App\Advantage;
use App\AdvantageContent;
use App\Certification;
use App\CompanyInNumber;
use App\MainBanner;
use App\MainTitle;
use App\OurClient;
use App\Service;
use App\ServiceRequest;
use App\ServiceStudyingProgram;

class HomeController extends Controller
{
    public function index(){

        $banner = MainBanner::getContent();
        $company_numbers = CompanyInNumber::getAll();
        $certificates = Certification::getAll();
        $advantage_con = AdvantageContent::getContent();
        $advantages = Advantage::getAll();
        $clients = OurClient::getAll();

        return view('home.index', compact('company_numbers', 'certificates',
            'advantage_con', 'advantages', 'banner', 'clients'));
    }

}
