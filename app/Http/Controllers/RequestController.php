<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Request as RequestForSite;
use App\BackCall as RequestForFeedback;
use App\ServiceRequest as RequestForService;
use App\Mail\FeedbackRequest;
use App\Mail\SiteRequest;
use App\Mail\ServiceRequest;

class RequestController extends Controller
{
    public function site(Request $request) {
        $validator = Validator::make($request->all(), [
            'fio' => 'required',
            'email' => 'required|email',
            'sms' => 'required',
            'agreement' =>'accepted'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForSite::create($request->all());
            Mail::to(setting('site.email'))->send(new SiteRequest($model));
            return response(['status' => 1], 200);
        }
    }


    public function feedback(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'telephone'    => 'required|regex:/(\+77)[0-9]{9}/',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForFeedback::create($request->all());
            Mail::to(setting('site.email'))->send(new FeedbackRequest($model));
            return response(['status' => 1], 200);
        }
    }

    public function service(Request $request) {
        $validator = Validator::make($request->all(), [
            'fio' => 'required',
            'email' => 'required|email',
            'training_purpose' => 'required',
            'service_studying_program_id' => 'required',
            'agreement' =>'accepted'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForService::create($request->all());
            Mail::to(setting('site.email'))->send(new ServiceRequest($model));
            return response(['status' => 1], 200);
        }
    }



}
