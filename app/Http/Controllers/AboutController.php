<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.02.2020
 * Time: 11:45
 */

namespace App\Http\Controllers;


use App\About;
use App\AboutType;
use App\Certification;
use App\OurClient;

class AboutController extends Controller
{

    public function index(){

        $about = About::getContent();
        $types = AboutType::getAll();
        $certificates = Certification::getAll();
        $clients = OurClient::getAll();

        return view('about.index', compact('about', 'types', 'certificates', 'clients'));
    }
}
