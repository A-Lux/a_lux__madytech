<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14.02.2020
 * Time: 16:14
 */

namespace App\Http\Controllers;


use App\News;
use App\NewsRecommend;

class BlogController extends Controller
{

    public function All(){
        $model = News::getAll();
        return view('blog.all', compact('model'));
    }

    public function Index($url){
        $model = News::getByUrl($url);
        if($model){
            $recommends = NewsRecommend::getByNews($model->id);
            return view('blog.index', compact('model', 'recommends'));
        }else{
            abort(404);
        }
    }
}
