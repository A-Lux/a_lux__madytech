<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.02.2020
 * Time: 12:02
 */

namespace App\Http\Controllers;


use App\PriceType;

class PriceController extends Controller
{
    public function index(){

        $prices = PriceType::getAll();
        return view('prices.index', compact('prices'));
    }

}
