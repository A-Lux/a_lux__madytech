<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 16.01.2020
 * Time: 15:37
 */

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AboutController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}
