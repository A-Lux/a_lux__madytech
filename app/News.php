<?php

namespace App;

use App\Page\IPage;
use Illuminate\Database\Eloquent\Model;


class News extends Model implements IPage
{
    public static function getAll(){
        return News::orderBy("sort")->get();
    }

    public static function getByUrl($url){
        return News::where('url', $url)->first();
    }
    
}
