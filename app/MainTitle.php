<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MainTitle extends Model
{
    public static function getAll(){
        return MainTitle::get();
    }

    public function getText(){
        return $this->text != null ? $this->text : "";
    }
}
